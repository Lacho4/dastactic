var mapSW = [0, 4096],
    mapNE = [4096, 0];
    // Declare map Object 
var map = L.map('map').setView([0, 0], 2);

var zoomHome = L.Control.zoomHome();
zoomHome.addTo(map);


    // Reference the Tiles
L.tileLayer('maps/{z}/{x}/{y}.png', {
    minZoom: 0,
    maxZoom: 4,
    continousWorld: false,
    noWrap: true,
    crs: L.CRS.Simple,
}).addTo(map);

//max granici na mapata
map.setMaxBounds(new L.LatLngBounds(
    map.unproject(mapSW, map.getMaxZoom()),
    map.unproject(mapNE, map.getMaxZoom())
    ));
    
 //   console.log("ghjghj!");

    // Icons
var myIconRed = L.icon({
    iconUrl: 'scripts/leafletJS/images/leaf-red.png',
    iconSize: [38, 95],
    iconAnchor: [22, 94],
    popupAnchor: [-3, -76],
    shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
    shadowSize: [41, 41],
    shadowAnchor: [5, 40]
});
//L.marker([50.505, 30.57], {icon: myIconRed}).addTo(map);
var myIconGreen = L.icon({
    iconUrl: 'scripts/leafletJS/images/leaf-green.png',
    iconSize: [38, 95],
    iconAnchor: [22, 94],
    popupAnchor: [-3, -76],
    shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
    shadowSize: [41, 41],
    shadowAnchor: [5, 40]
});
var markerZoom = L.icon({
    iconUrl: 'img/marker-mine.png',
   // iconSize: [38, 95],
    iconAnchor: [14, 14],
   // popupAnchor: [-3, -76],
   //  shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
   // shadowSize: [41, 41],
   // shadowAnchor: [5, 40]
});
var markerZoomHover = L.icon({
    iconUrl: 'img/marker-mine-hover.gif',
   // iconSize: [38, 95],
    iconAnchor: [14, 14],
   // popupAnchor: [-3, -76],
   //  shadowUrl: 'scripts/leafletJS/images/leaf-shadow.png',
   // shadowSize: [41, 41],
   // shadowAnchor: [5, 40]
});
var markerZoomSVG = L.icon({
    iconUrl: 'img/search1.svg',
    iconSize: [25,25],
    iconAnchor: [14,14]

});
    // Markers and Popups
var refMarker = L.marker([2, -70],{
    draggable: true,
}).addTo(map);
refMarker.bindPopup('');


    // Pixels
var marker_center = L.marker(map.unproject([2048, 2048], map.getMaxZoom())).addTo(map);
marker_center.bindPopup('markerCenter').openPopup();

refMarker.on('dragend', function(e) {
    refMarker.getPopup().setContent('Clicked ' + refMarker.getLatLng().toString() + '<br />'
    + 'Pixels ' + map.project(refMarker.getLatLng(), map.getMaxZoom().toString())).openOn(map);
});

var randomNumber = 0;

var addressPointsHead = [
    [73.378215, 0.175781, "markerHead" + randomNumber++],
    [71.413177, 0.087891, "markerHead" + randomNumber++],
    [69.503765, 5.800781, "markerHead" + randomNumber++],
    [70.466207, 9.228516, "markerHead" + randomNumber++],
    [67.609221, 0.703125, "markerHead" + randomNumber++],
    // [66.231457, -2.460938, "markerHead_6"],
    // [65.658275, -4.746094, "markerHead_7"],
    // [64.886265, -6.503906, "markerHead_8"],
    // [64.052978, 0.263672, "markerHead_9"],
    // [62.226996, -2.460938, "markerHead_10"],
    // [61.270233, 1.845703, "markerHead_11"],
    // [62.552857, 4.570313, "markerHead_12"],
    // [64.129784, 6.855469, "markerHead_13"],
    // [65.874725, 6.591797, "markerHead_14"],
    // [65.658275, 3.339844, "markerHead_15"],
    //// [64.886265, -6.503906, "markerHead_16"],
    // [58.309489, -2.109375, "markerHead_17"],
    // [54.72462, -10.458984, "markerHead_18"],
    // [54.059388, -5.888672, "markerHead_19"],
    // [56.656226, 1.669922, "markerHead_20"],
    // [54.41893, 3.251953, "markerHead_21"],
    // [55.078367, 10.634766, "markerHead_22"],

];  

  var markersHead = L.markerClusterGroup({
    showCoverageOnHover:true,
    animate:true,
    spiderfyOnMaxZoom: false,
    disableClusteringAtZoom: 3,
    maxClusterRadius:35
  });
  
  for (var i = 0; i < addressPointsHead.length; i++) {
    var a = addressPointsHead[i];
      var title = a[2];
      debugger;
      var marker = L.marker(new L.LatLng(a[0], a[1]), {
          alt: title,
          icon: markerZoom
      }).on('mouseover', function (e) {
          debugger;
          var markerAlt = e.target.options.alt;
          var markerElement = document.getElementById(markerAlt);
          markerElement.style.background = "black";
          e.target.setIcon(markerZoomHover);
      }).on('mouseout', function (e) {
          debugger;
          var markerAlt = e.target.options.alt;
          var markerElement = document.getElementById(markerAlt);
          markerElement.style.background = "white";
          e.target.setIcon(markerZoom)
      }).on('click', function (e) {

          var markerAlt = e.target.options.alt;
          var markerElement = document.getElementById(markerAlt);

          var test = document.querySelector("markerHead4");
          var sidebar = document.getElementById("sidebar");
          sidebar.classList.add("collapsed");
          sidebar.style.display = "none";
          var sidebar2 = document.getElementById("sidebar2");
          sidebar2.classList.remove("collapsed");

          e.target.setIcon(markerZoom)
      });
    marker.bindPopup(title);
    markersHead.addLayer(marker);
    
  }
  
  map.addLayer(markersHead);
  

  map.on("moveend", function(e){
    console.log("Hello world!");
  });


function getImagesByAlt(alt) {
    var allImages = document.getElementsByTagName("img");
    var images = [];
    for (var i = 0, len = allImages.length; i < len; ++i) {
        if (allImages[i].alt == alt) {
            images.push(allImages[i]);
        }
    }
    return images;
}
 
$(document).ready(function () {
    var sidebar2 = document.getElementById("sidebar2");
    sidebar2.classList.add("collapsed");
});
  
  function hovbeerrrr() {
   // debugger;
    var allParagraph = document.querySelectorAll("");
  
    for (var i in allParagraph) {
      var currentElement = allParagraph[i];
      currentElement.style.display = "";
    }
  }
  
  function hideFirstParagraph() {
   // debugger;
    var allParagraph = document.querySelectorAll(".even");
  
    for (var i in allParagraph) {
      var currentElement = allParagraph[i];
      currentElement.style.display = "none";
    }
  
    var allParagraph = document.querySelector("even");
    //firstParagraph.style.display = "none";
  }

  //marker1.addTo(map);